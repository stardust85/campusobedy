var deffer = function(action) {
    if (document.readyState === "complete" 
        || document.readyState === "loaded" 
        || document.readyState === "interactive") {

        action();

    } else {
        document.addEventListener("DOMContentLoaded", function() {
            action();
        });
    }
};

var get = function(url, done, elementId) {
    var req = new XMLHttpRequest();
    req.open('GET', url, true);
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if (req.status === 200
                /* network error, try parsing what we have anyway*/
                || req.status === 0) {

                try {
                    done(req.responseText);
                } catch (e) {
                    setError(elementId, e);
                }

            } else {
                setError(elementId, req);
            }
        }
    };
    req.send();
};

var setError = function(elementId, error) {
    console.warn(error);
    deffer(function() {
        var ph = document.getElementById(elementId);
        var p = document.createElement('p');
        p.innerText = 'Omlouváme se, ale denní menu není k dispozici';
        ph.parentElement.replaceChild(p, ph);
    });
};

var insertTable = function(elementId, table) {
    deffer(function() {
        var ph = document.getElementById(elementId);
        ph.parentElement.replaceChild(table, ph);
    });
}

var wrap = function(response) {
    var responseWrapper = document.createElement('div');
    var sanitizedResponse = /<body [^>]*>(.*)<\/body>/.exec(response.replace(/\n/g, ''));
    responseWrapper.innerHTML = (sanitizedResponse && sanitizedResponse[1] || response).replace(/<img [^>]*>/g, '');
    return responseWrapper;
}

var prepareTable = function(isSimple) {
    var table = document.createElement('table');

    if (isSimple === true) {
        return table;
    }

    var colGroup = document.createElement('colgroup');
    var col1 = document.createElement('col');
    var col2 = document.createElement('col');
    col1.setAttribute('span', 1);
    col1.style.cssText += 'width: 80%';
    col2.setAttribute('span', 1);
    col2.style.cssText += 'width: 20%;';
    colGroup.appendChild(col1);
    colGroup.appendChild(col2);
    table.appendChild(colGroup);
    return table;
}

var tableLines = function(table) {
    return Array.prototype.slice.call(table.getElementsByTagName('tr'));
}

var listLines = function(list) {
    return Array.prototype.slice.call(list.getElementsByTagName('li'));
}

get('/collectors/academic.php', function(response) {
    response = response.replace(/&lt;/g, '<').replace(/&gt;/g, '>');
    var begin = response.indexOf('<description><');
    var end = response.indexOf('></description>');
    var body = response.substring(begin + 13, end + 1);
    var responseWrapper = wrap(body);
    var lines = responseWrapper.getElementsByTagName('li');

    var table = prepareTable(true);
    var limitLine;
    for (var i = 0; i < lines.length; i++) {
        if (i === 8) {
            limitLine = row;
        }
        
        var row = table.insertRow();
        var cell = row.insertCell();
        cell.textContent = lines[i].innerText;
    }
    
    if (i > 12) {
        limitLine.insertAdjacentHTML('afterend', '<input type="checkbox" id="academicShowMoreBox" class="show-more"><label for="academicShowMoreBox" class="show-more">Zobrazit vše</label>');
    }

    insertTable('academic', table);
}, 'academic');

get('/collectors/campea.php', function(response) {
    var responseWrapper = wrap(response);
    var lines = tableLines(responseWrapper.getElementsByTagName('table')[0]);
    var dayIndex = (new Date().getDay() - 1) * 7;

    var table = prepareTable();
    for (var i = dayIndex + 1; i < dayIndex + 7; i++) {
        var row = table.insertRow();
        var cells = lines[i].getElementsByTagName('td');
        var jidlo = row.insertCell();
        jidlo.textContent = cells[1].innerText;
        var cena = row.insertCell();
        cena.textContent = cells[2].innerText;
    }

    insertTable('campea', table);
}, 'campea');

get('/collectors/husa.php', function(response) {
    var responseWrapper = wrap(response);
    var lines = listLines(responseWrapper.getElementsByClassName('menu-list')[0]);

    var table = prepareTable();
    for (var i = 0; i < lines.length; i++) {
        var row = table.insertRow();
        var jidlo = row.insertCell();
        jidlo.textContent = lines[i].getElementsByClassName('menu-list__name')[0].innerText;
        var cena = row.insertCell();
        cena.textContent = lines[i].getElementsByClassName('menu-list__price')[0].innerText.replace(',00', '');
    }

    insertTable('husa', table);
}, 'husa');

get('/collectors/jednabasen.php', function(response) {
    var lines = JSON.parse(response.substring(0, response.length - 1)).daily_menus[0].daily_menu.dishes;

    var table = prepareTable();
    for (var i = 0; i < lines.length; i++) {
        var dish = lines[i].dish;
        var row = table.insertRow();
        var jidlo = row.insertCell();
        jidlo.textContent = dish.name;
        var cena = row.insertCell();
        cena.textContent = dish.price;
    }

    insertTable('basen', table);
}, 'basen');

get('/collectors/malesklizeno.php', function(response) {
    var lines = JSON.parse(response.substring(0, response.length - 1)).daily_menus[0].daily_menu.dishes;

    var table = prepareTable();
    for (var i = 0; i < lines.length; i++) {
        var dish = lines[i].dish;
        var row = table.insertRow();
        var jidlo = row.insertCell();
        jidlo.textContent = dish.name;
        var cena = row.insertCell();
        cena.textContent = dish.price;
    }

    insertTable('malesklizeno', table);
}, 'malesklizeno');

get('/collectors/mixgrill.php', function(response) {
    var responseWrapper = wrap(response);
    var soup = responseWrapper.getElementsByClassName('soup')[0].getElementsByTagName('b')[0].innerText;
    var lines = listLines(responseWrapper.getElementsByClassName('food')[0].getElementsByTagName('ul')[0]);

    var table = prepareTable();

    var row = table.insertRow();
    var cell = row.insertCell();
    cell.textContent = 'Polévka: ' + soup;
    row.insertCell();

    for (var i = 0; i < lines.length; i++) {
        var row = table.insertRow();
        var jidlo = row.insertCell();
        jidlo.textContent = lines[i].getElementsByTagName('b')[0].innerText
            + ', ' + lines[i].getElementsByTagName('span')[0].innerText;
        var cena = row.insertCell();
        cena.textContent = lines[i].getElementsByTagName('strong')[0].innerText.replace(',–', ' Kč');
    }

    insertTable('mixgrill', table);
}, 'mixgrill');

get('/collectors/okruhpub.php', function(response) {
    var lines = JSON.parse(response.substring(0, response.length - 1)).daily_menus[0].daily_menu.dishes;

    var table = prepareTable();
    var prices;
    for (var i = 0; i < lines.length; i++) {
        var dish = lines[i].dish;

        if (!!~dish.name.indexOf('----')) {
            break;
        }

        if (!prices) {
            prices = {};
            var regx = /([A-Z]\d?(?:([-,])[A-Z]\d?)*)-(\d{2,3})(?:,-)?/g;
            var match;
            while ((match = regx.exec(dish.name)) !== null) {
                var letters = match[1].split(match[2] || ',');
                for (var j = 0; j < letters.length; j++) {
                    prices[letters[j].match(/\w/)[0].toLowerCase()] = match[3];
                }
            }

            dish.name = dish.name.match(/^(.+)\s[A-Z]\d?[,-]/)[1];
        }

        var row = table.insertRow();
        var jidlo = row.insertCell();
        jidlo.textContent = dish.name;

        var dishLetter = (dish.name.match(/\d?([a-zA-Z])\)\s/) || [])[1];
        var price = prices[dishLetter ? dishLetter.toLowerCase() : dishLetter];
        var cena = row.insertCell();
        cena.textContent = !!price ? (price + ' Kč') : '';
    }

    insertTable('okruh', table);
}, 'okruh');

get('/collectors/river.php', function(response) {
    var lines = JSON.parse(response.substring(0, response.length - 1)).daily_menus[0].daily_menu.dishes;

    var table = prepareTable();
    var row = table.insertRow();
    var cell = row.insertCell();
    cell.textContent = lines[0].dish.name;
    row.insertCell();

    var prev = '';
    for (var i = 1; i < lines.length; i++) {
        var dish = lines[i].dish;
        if (/H ?O ?T ?T ?I ?P/.test(dish.name)) {
            break;
        }
        if (/Týdenní menu/.test(dish.name)) {
            continue;
        }
        var parsed = /(?:(.+) )?(\d+),-( \(.+\))?/.exec(dish.name);
        if (!parsed) {
            prev += dish.name + ' ';
            continue;
        }

        var row = table.insertRow();
        var jidlo = row.insertCell();
        jidlo.textContent = prev + parsed[1] + parsed[3];
        var cena = row.insertCell();
        cena.textContent = parsed[2] + ' Kč';

        prev = '';
    }

    insertTable('river', table);
}, 'river');

get('/collectors/campusbistro.php', function(response) {
    var responseWrapper = wrap(response);
    var lines = responseWrapper
            .getElementsByClassName('menu-col')[0]
            .querySelectorAll('.row.meal');

    var table = prepareTable();
    for (var i = 0; i < lines.length; i++) {
        var row = table.insertRow();
        var name = lines[i].getElementsByClassName('name')[0];
        var num = name.firstChild.innerText;
        num = num ? num + '. ' : 'Polévka: ';

        var jidlo = row.insertCell();
        jidlo.textContent = num
            + name
                .lastChild
                .textContent
                .replace(/\n|\s{2,}/g, '');

        var cena = row.insertCell();
        cena.textContent = lines[i]
                .getElementsByClassName('price')[0]
                .innerText
                .replace(/\n|\s{2,}/g, '')
                .replace(',-', ' Kč');
    }

    insertTable('campusbistro', table);
}, 'campusbistro');